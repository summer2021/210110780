# 重构任务系统-任务拆分

* 实现开发基础条件

  - [x] 环境搭建（Go/Database/MinIO/Redis）

  - [x] 知识储备（Golang/EduOJ整体了解）
* 设计重构方案

  - [x] 对现有系统拆分，总结当前Run的创建、排序、运行方式
      - 创建方法，用户提交submission，submission中进行新run的创建
      - run依次通过成员变量priority、id进行排序
      - 运行时评测机从当前任务队列中选择第一个未被评测的任务进行一系列评测操作
      
  - [x] 设计新的任务系统，支持多种任务，同时维持Run的正常处理

  - [x] 对比新旧两套方案，分析总结方案转换间需要的工作

  - [x] 注：方案包括数据库表结构设计、流程设计等，推荐使用流程图等图示方式表示

      - 旧方案的工作方式是用户创建一个提交请求，其中包含很多个run对象。当前任务队列以Run为元素，创建类Task以及元素为Task的任务队列，代替Run的任务队列。
      - 新方案要求评测机能支持多种任务如代码查重、输出比对、代码静态分析等功能。


      **系统重构需要做的工作**

      后端：

      - 新建类Task，结构如下所示。后端构建以Task为元素的任务队列。

          ```go
          type Task struct {
          	ID uint `gorm:"primaryKey" json:"id"`
          
          	TaskType string `json:"task_type"`
          	Priority uint8  `json:"priority"`
          	Detail   string   `json:"detail"`
          }
          ```

          Detail变量为string类型，由`json.Marshal()`将具体任务对象的结构体转化为json后再经`string()`类型转化为string类写入Task结构体。

      - `GetTask()`控制器：主逻辑不变，`getRun()`函数应写为`getTask()`函数，在`getTask()`函数中从当前任务数据库中取出优先级最高的一条；修改`generateResponse()`函数，该函数根据获得任务的种类switch到不同的Response产生函数如`generateRunTaskResponse()`。

          <font color="red">也可直接将Task中的内容返回</font>

      评测机端：

      - 将原有的Task结构体扩写出多个任务类型的结构体，如`RunTask`，`CompareTask`...根据`GetTask()`收到的返回值中TaskType构造出对应的任务类型结构体，进行相应任务评测。
      - 保留现有的`UpdateRun()`接口，后续若需要不同Update接口，则在后端路由规则中进行添加并实现相应更新函数。

      为保证原有`Run`任务正常执行需要进行的改动：

      - 用户创建Submission生成Run的同时生成对应`Task`，且`TaskType = RunTask`
      - `getTask()`与` generateResponse()`对应修改见**后端**部分
      - 评测机端原有的Task类现在应为RunTask类，后续若添加不同任务需添加相应类。评测机在接受到`GetTask()`返回值后根据任务类型switch到相应任务的执行函数。
      - 评测机端原有的`UpdateRun()`函数不变，后续根据需要自行添加相应接口与函数。
* 外特性定义与测试修订

  - [ ] 明确定义重构对象（Run处理功能）的外特性

  - [ ] Run的创建与更新

  - [ ] Run的处理优先级

  - [ ] Run相关API

  - [ ] 修订现有测试，使其测试外特性而不涉及内部实现细节（若过于难以修改的test可放弃，但需保证一定数量有效test）
* 重构Run处理功能与开发任务系统（主要部分）

  - [x] 开发任务系统，开发同时提供（可以较为简略但）易于理解的文档便于其他开发者使用

  - [ ] 重构Run处理功能，修改时频繁使用之前修订的测试，保证Run处理功能正常
* 完善代码与文档

  - [ ] 为覆盖率较低的部分补充test，保证达到不低于既有代码的覆盖率

  - [ ] 将代码风格与既有代码统一（变量命名等，当然最好是一开始开发的时候就统一）

  - [ ] 完善文档，最好自己按照文档说明开发一个简单的任务作为样例，测试系统正确性的同时也展示了用法


