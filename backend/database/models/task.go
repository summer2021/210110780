package models

import (
	"github.com/EduOJ/backend/base"
	"gorm.io/gorm"
	"gorm.io/datatypes"
	"time"
)

type Task struct {
	ID        uint             `gorm:"primaryKey" json:"id"`

	/*
	 * Run, Compare...
	 */
	TaskType            string           `json:"task_type"`
	Priority            uint8            `json:"priority"`
	Status              string           `json:"status"`
	JudgerName          string           
	JudgerMessage       string 
	Detail              datatypes.JSON   `json:"detail"`

	CreatedAt           time.Time        `json:"created_at"`
	UpdatedAt           time.Time        `json:"-"`
	DeletedAt           gorm.DeletedAt   `json:"deleted_at"`
}